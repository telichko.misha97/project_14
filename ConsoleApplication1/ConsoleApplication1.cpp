﻿
#include <iostream>
#include <string>

int main()
{
    std::string month;
    month = "june";

    std::cout << month << "\n";
    std::cout << month.length() << "\n";
    std::cout << "first character = " << month.front() << "\n";
    std::cout << "last character = " << month.back() << "\n";
}
